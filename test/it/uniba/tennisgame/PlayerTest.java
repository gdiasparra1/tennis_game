package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void scoreShouldBeIncreased() { //caso in cui incremento prima classe equivalenza
		//Arrange
		Player player= new Player("Federer", 0);
		//Act
		player.incrementScore();
		//Assert
		assertEquals(1,player.getScore());
	}

	@Test
	public void scoreShouldNotBeIncreased() { //caso in cui non incremento, seconda classe equivalenza
		//Arrange
		Player player= new Player("Federer", 0);
		//Act
		//non devo fare niente
		//Assert
		assertEquals(0,player.getScore());
	}
	
	
	@Test
	public void scoreShouldBeLove() { //per classe 0
		//Arrange
		Player player= new Player("Federer", 0);
		//Act
		String scoreAsString=player.getScoreAsString();
		//Assert
		assertEquals("love",scoreAsString);
	}
	
	@Test
	public void scoreShouldBeFifteen() { //per classe 1
		//Arrange
		Player player= new Player("Federer", 1);
		//Act
		String scoreAsString=player.getScoreAsString();
		//Assert
		assertEquals("fifteen",scoreAsString);
	}
	
	@Test
	public void scoreShouldBeThirty() { //per classe 2
		//Arrange
		Player player= new Player("Federer", 2);
		//Act
		String scoreAsString=player.getScoreAsString();
		//Assert
		assertEquals("thirty",scoreAsString);
	}
	
	@Test
	public void scoreShouldBeForty() { //per classe 3
		//Arrange
		Player player= new Player("Federer", 3);
		//Act
		String scoreAsString=player.getScoreAsString();
		//Assert
		assertEquals("forty",scoreAsString);
	}
	
	@Test
	public void scoreShouldBeNullIfNegative() { //per classe invalida -1
		//Arrange
		Player player= new Player("Federer", -1);
		//Act
		String scoreAsString=player.getScoreAsString();
		//Assert
		assertNull(scoreAsString);
	}
	
	
	@Test
	public void scoreShouldBeNullIfMoreThanThree() { //per classe invalida non traducibile
		//Arrange
		Player player= new Player("Federer", 4);
		//Act
		String scoreAsString=player.getScoreAsString();
		//Assert
		assertNull(scoreAsString);
	}
	
	

	@Test
	public void ShouldBeTie() {
		//Arrange
		Player player1=new Player("Federer", 2);
		Player player2=new Player("Nadal", 2);
		
		//Act
		boolean tie=player1.isTieWith(player2);
		
		//Assert
		assertTrue(tie);
	}
	
	@Test
	public void ShouldNotBeTie() {
		//Arrange
		Player player1=new Player("Federer", 3);
		Player player2=new Player("Nadal", 2);
		
		//Act
		boolean tie=player1.isTieWith(player2);
		
		//Assert
		assertFalse(tie);
	}
	
	
	@Test
	public void ShouldHaveAtLeastFortyPoints() {
		//Arrange
		Player player1=new Player("Federer", 3);
		//Act
		boolean outcome=player1.hasAtLeastFortyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	@Test
	public void ShouldNotHaveAtLeastFortyPoints() {
		//Arrange
		Player player1=new Player("Federer", 2);
		//Act
		boolean outcome=player1.hasAtLeastFortyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	

	@Test
	public void ShouldHaveLessThanFortyPoints() {
		//Arrange
		Player player1=new Player("Federer", 2);
		//Act
		boolean outcome=player1.hasLessThanFortyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	
	
	@Test
	public void ShouldNotHaveLessThanFortyPoints() {
		//Arrange
		Player player1=new Player("Federer", 3);
		//Act
		boolean outcome=player1.hasLessThanFortyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	
	
	
	@Test
	public void ShouldHaveMoreThanFourtyPoints() {
		//Arrange
		Player player1=new Player("Federer", 4);
		//Act
		boolean outcome=player1.hasMoreThanFourtyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	
	@Test
	public void ShouldNotHaveMoreThanFourtyPoints() {
		//Arrange
		Player player1=new Player("Federer", 3);
		//Act
		boolean outcome=player1.hasMoreThanFourtyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	
	
	
	
	
	@Test
	public void ShouldHaveOnePointAdvantageOn() {
		//Arrange
		Player player1=new Player("Federer", 4);
		Player player2=new Player("Nadal", 3);
		//Act
		boolean outcome=player1.hasOnePointAdvantageOn(player2);
		//Assert
		assertTrue(outcome);
	}
	
	
	@Test
	public void ShouldNotHaveOnePointAdvantageOn() {
		//Arrange
		Player player1=new Player("Federer", 3);
		Player player2=new Player("Nadal", 3);
		//Act
		boolean outcome=player1.hasOnePointAdvantageOn(player2);
		//Assert
		assertFalse(outcome);
	}
	
	
	
	@Test
	public void ShouldHaveAtLeastTwoPointsAdvantageOn() {
		//Arrange
		Player player1=new Player("Federer", 5);
		Player player2=new Player("Nadal", 3);
		//Act
		boolean outcome=player1.hasAtLeastTwoPointsAdvantageOn(player2);
		//Assert
		assertTrue(outcome);
	}
	
	
	@Test
	public void ShouldNotHaveAtLeastTwoPointsAdvantageOn() {
		Player player1=new Player("Federer", 4);
		Player player2=new Player("Nadal", 3);
		//Act
		boolean outcome=player1.hasAtLeastTwoPointsAdvantageOn(player2);
		//Assert
		assertFalse(outcome);
	}
	
	
	
}
