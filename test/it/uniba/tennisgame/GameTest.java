package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void testFifteenThirty() throws Exception {	//caso 1
			
		//Arrange
		 Game game = new Game("Federer", "Nadal");
		 String playerName1 = game.getPlayerName1(); 
		 String playerName2 = game.getPlayerName2();
		 
		 
		 //Act
		 game.incrementPlayerScore(playerName1);
		 
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 
		 String status = game.getGameStatus();
		 
		//Assert
		 assertEquals("Federer fifteen - Nadal thirty",status);
	}
	
	@Test
	public void testFortyThirty() throws Exception {	//caso 2
		
		//Arrange
		 Game game = new Game("Federer", "Nadal");
		 String playerName1 = game.getPlayerName1(); 
		 String playerName2 = game.getPlayerName2();
		 
		 
		 //Act
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 
		 String status = game.getGameStatus();
		 
		//Assert
		 assertEquals("Federer forty - Nadal thirty",status);
	}
	
	
	
	@Test
	public void testPlayer1Wins() throws Exception {	//caso 3, il test fallisce perch� giocatore uno aveva gi� vinto quindi non posso incrementare
		
		//Arrange
		 Game game = new Game("Federer", "Nadal");
		 String playerName1 = game.getPlayerName1(); 
		 String playerName2 = game.getPlayerName2();
		 
		 
		 //Act
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);

		 
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 
		 //quindi sposto queste sotto per non sollevare l'eccezione che non mi faceva incrementare perch� avevo gi� vinto
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 
		 String status = game.getGameStatus();
		 
		//Assert
		 assertEquals("Federer wins",status);
	}
	
	
	
	@Test
	public void testFifteenForty() throws Exception {	//caso 4
		//Arrange
		 Game game = new Game("Federer", "Nadal");
		 String playerName1 = game.getPlayerName1(); 
		 String playerName2 = game.getPlayerName2();
		 
		 
		 //Act
		 game.incrementPlayerScore(playerName1);

		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);

		 
		 String status = game.getGameStatus();
		 
		//Assert
		 assertEquals("Federer fifteen - Nadal forty",status);
	}
	
	
	
	
	@Test
	public void testDeuce() throws Exception {	//caso 5
		//Arrange
		 Game game = new Game("Federer", "Nadal");
		 String playerName1 = game.getPlayerName1(); 
		 String playerName2 = game.getPlayerName2();
		 
		 
		 //Act
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);

		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);

		 
		 String status = game.getGameStatus();
		 
		//Assert
		 assertEquals("Deuce",status);
	}
	
	
	
	
	@Test
	public void testAdvantagePlayer1() throws Exception {	//caso 6
		//Arrange
		 Game game = new Game("Federer", "Nadal");
		 String playerName1 = game.getPlayerName1(); 
		 String playerName2 = game.getPlayerName2();
		 
		 
		 //Act
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);

		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 
		 game.incrementPlayerScore(playerName1);

		 
		 String status = game.getGameStatus();
		 
		//Assert
		 assertEquals("Advantage Federer",status);
	}

	
	
	
	@Test
	public void testPlayer2Wins() throws Exception {	//caso 7
		//Arrange
		 Game game = new Game("Federer", "Nadal");
		 String playerName1 = game.getPlayerName1(); 
		 String playerName2 = game.getPlayerName2();
		 
		 
		 //Act
		 game.incrementPlayerScore(playerName1);

		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);		 
		 game.incrementPlayerScore(playerName2);

		 
		 String status = game.getGameStatus();
		 
		//Assert
		 assertEquals("Nadal wins",status);
	}
	
	
	
	
	@Test
	public void testAdvantagePlayer2() throws Exception {	//caso 8
		//Arrange
		 Game game = new Game("Federer", "Nadal");
		 String playerName1 = game.getPlayerName1(); 
		 String playerName2 = game.getPlayerName2();
		 
		 
		 //Act
		 game.incrementPlayerScore(playerName1);
		 
		 game.incrementPlayerScore(playerName2);
		 
		 game.incrementPlayerScore(playerName1);

		 game.incrementPlayerScore(playerName2);
		 
		 game.incrementPlayerScore(playerName1);
		 
		 game.incrementPlayerScore(playerName2);
		 		 
		 game.incrementPlayerScore(playerName2);

		 
		 String status = game.getGameStatus();
		 
		//Assert
		 assertEquals("Advantage Nadal",status);
	}


	
	@Test
	public void testDeuceAfterAdvantage() throws Exception {	//caso 9
		//Arrange
		 Game game = new Game("Federer", "Nadal");
		 String playerName1 = game.getPlayerName1(); 
		 String playerName2 = game.getPlayerName2();
		 
		 
		 //Act
		 game.incrementPlayerScore(playerName1);
		 
		 game.incrementPlayerScore(playerName2);
		 
		 game.incrementPlayerScore(playerName1);

		 game.incrementPlayerScore(playerName2);
		 
		 game.incrementPlayerScore(playerName1);
		 
		 game.incrementPlayerScore(playerName2);
		 
		 game.incrementPlayerScore(playerName1);
		 		 
		 game.incrementPlayerScore(playerName2);

		 
		 String status = game.getGameStatus();
		 
		//Assert
		 assertEquals("Deuce",status);
	}
	
}
